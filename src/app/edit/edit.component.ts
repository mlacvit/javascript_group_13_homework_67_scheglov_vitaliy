import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FoodModel } from '../food.model';
import { FoodService } from '../food.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  isEdit = false;
  editId = '';
  @ViewChild('formFood') formFood!: NgForm;

  constructor(private service: FoodService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe( data => {
      const food = <FoodModel>data['food'];
      if (food) {
        this.isEdit = true;
        this.editId = food.id;
        this.setFormValue({
          callories: food.callories,
          category: food.category,
          eat: food.eat
        });
      }else {
        this.isEdit = false;
        this.editId = '';
        this.setFormValue({
          callories: '',
          category: '',
          eat: ''
        });
      }
    });
  }

  setFormValue(value: {[key: string]: any}){
    setTimeout(() => {
      this.formFood.form.setValue(value);
    });
  }

  editFood() {
  const id = this.editId || Math.random().toString();
  const food = new FoodModel(
    id,
    this.formFood.value.callories,
    this.formFood.value.category,
    this.formFood.value.eat
  );

  const next = () => {
    this.service.getFoods();
    void this.router.navigate(['/'])
  };

  if (this.isEdit){
    this.service.editFood(food).subscribe(next);
  } else {
    this.service.addFood(food).subscribe(next);
  }
  };
}
