import { Injectable } from '@angular/core';
import { FoodModel } from './food.model';
import { map, Subject, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FoodService {
  foods: FoodModel[] | null = null;
  changeFood = new Subject<FoodModel[]>()
  foodFetching = new Subject<boolean>();
  foodRemove = new Subject<boolean>();
  foodEditer = new Subject<boolean>();
  constructor(private http: HttpClient) {
  }

  getFoods() {
    this.foodFetching.next(true);
    this.http.get<{[id: string]: FoodModel}>('https://mlacvit-10af9-default-rtdb.firebaseio.com/foods.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new FoodModel(id, data.callories, data.category, data.eat)
        })
      }))
      .subscribe((food: FoodModel[]) => {
        this.foods = food;
        this.changeFood.next(this.foods.slice());
        this.foodFetching.next(false);
      }, error => {
        this.foodFetching.next(false);
      });
  };

  foodOne(id: string) {
    return this.http.get<FoodModel | null>(`https://mlacvit-10af9-default-rtdb.firebaseio.com/foods/${id}.json`)
      .pipe(map(result => {
        if (!result) {
          return null;
        }
        return new FoodModel(id, result.callories, result.category, result.eat);
      }));
  };

  addFood(food: FoodModel) {
    const body = {
      callories: food.callories,
      category: food.category,
      eat: food.eat
    };
    this.foodEditer.next(true);

    return this.http.post(`https://mlacvit-10af9-default-rtdb.firebaseio.com/foods.json`, body).pipe(
      tap(() =>{
        this.foodEditer.next(false);
      }, error => {
        this.foodEditer.next(false);
      })
    );
  }

  editFood(food: FoodModel) {
    this.foodEditer.next(true);
    const body = {
      callories: food.callories,
      category: food.category,
      eat: food.eat
    };
    return this.http.put(`https://mlacvit-10af9-default-rtdb.firebaseio.com/foods/${food.id}.json`, body).pipe(
      tap(() =>{
        this.foodEditer.next(false);
      }, error => {
        this.foodEditer.next(false);
      })
    );
  };

  removeFood(id: string){
    this.foodRemove.next(true);
    return this.http.delete(`https://mlacvit-10af9-default-rtdb.firebaseio.com/foods/${id}.json`).pipe(
      tap(() =>{
        this.foodRemove.next(false);
      }, error => {
        this.foodRemove.next(false);
      })
    );
  }


}
