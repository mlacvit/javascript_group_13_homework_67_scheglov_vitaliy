import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
import { ResolveService } from './resolve.service';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'new', component: EditComponent},
  {
    path: ':id/edit',
    component: EditComponent,
    resolve: {food: ResolveService}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
