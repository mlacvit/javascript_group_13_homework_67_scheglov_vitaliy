import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FoodModel } from '../food.model';
import { FoodService } from '../food.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  foods: FoodModel[] | null = null;
  foodSubChange!: Subscription;
  foodFetchingSubscriptions!: Subscription;
  isFetching: boolean = false;
  remSubFood!: Subscription;
  removing = false;
  result = 0;

  constructor(public service: FoodService, private router: Router) {}

  ngOnInit(): void {
    this.service.getFoods();

    this.foodFetchingSubscriptions = this.service.foodFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });

    this.foodSubChange = this.service.changeFood.subscribe((food: FoodModel[]) => {
      this.foods = food;
    });

    this.remSubFood = this.service.foodRemove.subscribe((rem: boolean) => {
      this.removing = rem;
    });
  }

  onRemove(id: string){
    this.service.removeFood(id).subscribe(() => {
      this.service.getFoods();
      void this.router.navigate(['/']);
    })
  }


  ngOnDestroy(): void {
    this.foodFetchingSubscriptions.unsubscribe();
    this.foodSubChange.unsubscribe();
    this.remSubFood.unsubscribe();
  }

}
