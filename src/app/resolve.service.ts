import { Injectable } from '@angular/core';
import { FoodService } from './food.service';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { FoodModel } from './food.model';
import { EMPTY, mergeMap, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResolveService implements Resolve<FoodModel> {

  constructor(private service: FoodService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FoodModel> | Observable<never> {
    const Id = route.params['id'];
    return this.service.foodOne(Id).pipe(mergeMap(food => {
      if (food){
        return of(food)
      }else {
        void this.router.navigate(['/']);
        return EMPTY;
      }
    }));
  }
}
